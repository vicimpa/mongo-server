/// <reference types="node" />

declare module "mongo-server" {
    export function on(...args: any[]): void;
    export function once(...args: any[]): void;
    export function off(name: string, callback: (...args: any[]) => void): void;
    export function run(): Promise<MongoOptions>;
    export function setOption(name: 'dbpath' | 'port' | 'host' | 'bin', value: string | number): string | number;
}
import * as EventEmiter from 'events';
import { ChildProcess } from 'child_process';
export declare class MongoOptions {
    dbpath: string;
    port: number;
    host: string;
    bin: string;
    set(name: 'dbpath' | 'port' | 'host' | 'bin', value: string | number): number | string;
    readonly mongodbString: string;
}
export declare const fsDeleteFolder: (folder: string) => Promise<void>;
export declare class MongoServer {
    static evets: EventEmiter;
    static options: MongoOptions;
    static process: ChildProcess;
    static logout: string;
    static _runned: boolean;
    static run(): Promise<MongoOptions>;
    static setOption(name: 'dbpath' | 'port' | 'host' | 'bin', value: string | number): string | number;
    static on(...args: any[]): void;
    static once(...args: any[]): void;
    static off(name: string, callback: (...args: any[]) => void): void;
}
