# MongoDB Server for projects

### Using global
```shell
    npm install mongo-server -g

    mongo-server
```

### Using local
```shell
    npm install mongo-server --save
```

```js
    const path = require('path')
    const mongoServer = require('mongo-server')

    let serverOprions = {
        dbPath: path.join(__dirname, 'database'),
        port: 27017,
        host: '127.0.0.1'
    }

    mongoServer.start(serverOprions)
        .then(() => {
            // Successful launch
        })
        .catch(error => {
            // Incorrect start
        })
```