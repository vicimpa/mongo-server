import * as path from 'path'
import * as fs from 'fs-extra-promise'
import * as EventEmiter from 'events'
import { spawn, ChildProcess } from 'child_process'


export class MongoOptions{
    public dbpath: string = path.join(__dirname, 'database')
    public port: number = 27017
    public host: string = '127.0.0.1'
    public bin: string = path.join(__dirname, './libs/server/mongod')

    public set(name: 'dbpath' | 'port' | 'host' | 'bin', value: string | number): number | string{
        if(!this[name])
            throw new Error(`Property with name '${name}' not found`)
        if(typeof this[name] !== typeof value)
            throw new Error(`Invalid data type for '${name}' property. This property needs a type ${typeof this[name]}`)
            
        if((name === 'dbpath' || name === 'bin') && typeof value === 'string'){
            let segments: string[] = []

            for(let segment of value.split('/')){
                segment = segment.replace(/DIR/g, __dirname)
                segment = segment.replace(/CWD/g, process.cwd())
                segment = segment.replace(/\./g, process.cwd())

                segments.push(segment)
            }

            value = path.join(...segments)
        }

        if(name === 'host' && typeof value === 'string'){
            if(!/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/.test(value))
                throw new Error(`Invalid 'host' property value. Example: '127.0.0.1'`)
        }

        if(name === 'port' && typeof value === 'number'){
            if(value <= 80 || value > 65535)
                throw new Error(`Invalid 'port' property value. Port value is integer 81-65535 numbers`)
        }

        this[name] = value
        return value
    }

    get mongodbString(): string{
        return `mongodb://${this.host}:${this.port}`
    }
}

export const fsDeleteFolder = async (folder: string) => {
    if (await fs.existsAsync(folder)) {
        let files = await fs.readdirAsync(folder)
        for(let file of files){
            let curPath = path.join(folder, file)
            if((await fs.lstatAsync(curPath)).isDirectory()){
                await fsDeleteFolder(curPath)
            }else{
                await fs.unlinkAsync(curPath)
            }
        }

        await fs.rmdirAsync(folder)
    }
  }

export class MongoServer{
    public static evets: EventEmiter = new EventEmiter()
    public static options: MongoOptions = new MongoOptions()
    public static process: ChildProcess
    public static logout: string = ''
    public static _runned: boolean = false

    public static run(): Promise<MongoOptions>{
        let that = MongoServer
        let options = that.options
        let events = that.evets

        if(that.process && !that.process.killed){
            that.process.kill('STOPED_SERVER')
            that.process = null
        }
        
        async function start(): Promise<MongoOptions>{
            if(!await fs.existsAsync(options.dbpath)){
                await fs.mkdirAsync(options.dbpath)
            }else{
                let stat = await fs.lstatAsync(options.dbpath)
                if(!stat.isDirectory()){
                    await fs.unlinkAsync(options.dbpath)
                    await fs.mkdirAsync(options.dbpath)
                }else{
                    for(let file of await fs.readdirAsync(options.dbpath)){
                        if(file.indexOf('mongod.lock') !== -1)
                            await fs.unlinkAsync(path.join(options.dbpath, file))
                    }
                    
                    await fsDeleteFolder(path.join(options.dbpath, 'diagnostic.data'))
                }
            }
            let argvs: string[] = []
              
            argvs.push(`--dbpath=${options['dbpath']}`)
            argvs.push(`--port=${options.port}`)

            that.process = spawn(options.bin, argvs)

            return new Promise<MongoOptions>((resolve, reject) => {
                that.process.once('close', async () => {
                    let logPath: string = path.join(process.cwd(), 'startLog.log')
                    let error = new Error(`The database started with an error. ReadMode in ${logPath}`)

                    await fs.writeFileAsync(logPath, that.logout, 'utf-8')

                    events.emit('error', error)
                    reject(error) 
                })

                that.process.stdout.on('data', (data) => {
                    that.logout += data.toString()

                    if(!that._runned){
                        if(that.logout.indexOf('waiting for connections') !== -1){
                            that._runned = true
                            events.emit('ready', options)
                            resolve(options)
                        }
                    }
                })
            })
        }

        return new Promise<MongoOptions>((resolve, reject) => {
            start().then(resolve, reject)
        })
    }

    public static setOption(name: 'dbpath' | 'port' | 'host' | 'bin', value: string | number): string | number{
        return MongoServer.options.set(name, value)
    }

    public static on(...args: any[]): void{
        MongoServer.evets.on.apply(MongoServer.evets, args)
    }
    public static once(...args: any[]): void{
        MongoServer.evets.once.apply(MongoServer.evets, args)
    }

    public static off(name: string, callback: (...args: any[]) => void){
        MongoServer.evets.removeListener(name, callback)
    }
}