#!/usr/bin/env node

import * as argv from 'argv'
import { MongoServer } from '../main'

for(let i = 0; i < process.argv.length; i++){
    let arg = process.argv[i]

    if(arg[0] === '-'){
        let value: string | number = process.argv[++i] || null
        value = isNaN(+value) ? value : +value
        MongoServer.options.set(<'dbpath'|'port'|'host'>arg.substr(1), value)
    }
}

MongoServer.run()
    .then((options) => {
        console.log('Server success running on port:', options.port)
        console.log(`Use url: '${options.mongodbString}' for connect`)
        MongoServer.on('log', (string: string) => {
            console.log(string) 
        })
    })
    .catch(console.error)