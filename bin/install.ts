import * as os from 'os'
import * as path from 'path'
import * as fs from 'fs-extra-promise'
import {spawn} from 'child_process'

function modeNum(m: number | string): string | number {
    switch (typeof m) {
        case 'number': 
            return m
        case 'string': 
            return parseInt(<string>m, 8)
    }
}

(async function install(): Promise<string>{
        if(os.type() === 'Linux')
            spawn('chmod', ['+x', '/usr/lib/node_modules/mongo-server/libs/**/*'])

        let dirs: string[] = [
            path.join(__dirname, '../libs/server/mongod'),
            path.join(__dirname, '../libs/server/mongorestore')
        ]
    
        let started = dirs.length
    
        if(os.type() === 'Linux')
            for(let dir of dirs)
                spawn('chmod', ['+x', dir])
                
            for(let dir of dirs)
                await fs.chmodAsync(dir, modeNum(777))

        return 'Its ok'
})().then().catch(error => {throw error})